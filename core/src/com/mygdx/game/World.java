package com.mygdx.game;

import java.util.Random;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class World {

    private BubbleGame bubbleGame;
    private Bubble bubble;
    private BathubLeft bathubLeft;
    private BathubRight bathubRight;
    private BubbleQueue bubbleQueue;
    private BubbleQueue2 bubbleQueue2;
    private double lastTimeCreateBubble;
    private int score;
    private float time;
    private int combo;
    private int distantBall;
    private static int countBub;
    private int checkcorrect;
    private SpriteBatch batch;

    public World(BubbleGame bubbleGame) {
        this.bubbleGame = bubbleGame;
        this.batch = bubbleGame.batch;
        this.bathubLeft = new BathubLeft(0, 0);
        this.bathubRight = new BathubRight(224, 0);
        this.bubbleQueue = new BubbleQueue();
        this.bubbleQueue2 = new BubbleQueue2();
        this.lastTimeCreateBubble = -1;
        this.score = 0;
        this.time = (float) 100.0;
        this.countBub = 0;
        this.distantBall = 0;
        this.checkcorrect = 0;

    }

    public BathubLeft getBathubLeft() {
        return this.bathubLeft;
    }

    public BathubRight getBathubRight() {
        return this.bathubRight;
    }

    public BubbleQueue getBubbleQueue() {
        return this.bubbleQueue;
    }

    public BubbleQueue2 getBubbleQueue2() {
        return this.bubbleQueue2;
    }

    public void RandomNumber() {
        if (World.getCountBub(this.countBub) >= 90) {
            this.distantBall = 3000;
        } else if (World.getCountBub(this.countBub) >= 70) {
            this.distantBall = 100;
        } else if (World.getCountBub(this.countBub) >= 50) {
            this.distantBall = 500;
        } else if (World.getCountBub(this.countBub) >= 30) {
            this.distantBall = 1300;
        } else {
            this.distantBall = 2500;
        }
    }

    public void updateQueueTimeLeft() {
        RandomNumber();
        Random random = new Random();
        int number = random.nextInt(this.distantBall) + 500;
        if (System.currentTimeMillis() - this.lastTimeCreateBubble >= number) {
            Bubble.Color bubbleColor = Bubble.getRandomColor();
            this.bubbleQueue.createBubble(60, 1330, bubbleColor);
            this.lastTimeCreateBubble = System.currentTimeMillis();
        }
        updateBubbleQueueLeft();
    }

    public void updateQueueTimeRight() {
        Random random = new Random();
        int number = random.nextInt(this.distantBall) + 500;
        if (System.currentTimeMillis() - this.lastTimeCreateBubble >= number) {
            Bubble.Color bubbleColor = Bubble.getRandomColor();
            this.bubbleQueue2.createBubble(250, 1330, bubbleColor);
            this.lastTimeCreateBubble = System.currentTimeMillis();
        }

        updateBubbleQueueRight();
    }

    public void updateBubbleQueueLeft() {
        for (int i = this.bubbleQueue.getfront();; i++) {
            if (i == this.bubbleQueue.getrear()) {
                break;
            }
            if (i == this.bubbleQueue.getSizeOfQueue()) {
                i = -1;
                continue;
            }
            this.bubbleQueue.getBubbleAt(i).update();
        }
    }

    public void updateBubbleQueueRight() {
        for (int i = this.bubbleQueue2.getfront();; i++) {
            if (i == this.bubbleQueue2.getrear()) {
                break;
            }
            if (i == this.bubbleQueue2.getSizeOfQueue()) {
                i = -1;
                continue;
            }
            this.bubbleQueue2.getBubbleAt(i).update();
        }
    }

    public int getScore() {
        return this.score;
    }

    public void increaseScore() {
        this.countBub += 1;
        this.score += 5;
        this.time += 0.9;
        this.combo += 1;
        checkCombo();
        this.checkcorrect = 1;
    }

    public void checkCombo() {
        if (this.combo == 8) {
            increaseTime();
            this.combo = 0;
        }
    }

    public void decreaseScore() {
        this.countBub += 1;
        this.score -= 1;
        this.time -= 1.5;
        this.combo = 0;
        this.checkcorrect = 0;
    }

    public float getTime() {
        return this.time;
    }

    public void increaseTime() {
        this.time += 12;
    }

    public void decreaseTime() {
        this.time -= 0.005;
    }

    public static float getCountBub(int count) {
        count = countBub;
        return count;
    }

    public float getCorrect() {
        return this.checkcorrect;
    }
}
