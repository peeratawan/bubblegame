package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.BitmapFontData;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.BathubLeft.Color;

public class WorldRenderer {

    private BubbleGame bubbleGame;
    private BubbleQueue bubbleQueue;
    private BubbleQueue2 bubbleQueue2;
    private Texture bubbleImg;
    private Texture bubblePinkImg;

    World world;
    private Texture backgroundImg;
    private Texture titleImg;
    private BathubLeft bathubLeft;
    private BathubRight bathubRight;

    private Texture bathubLeftImg;
    static Texture bathubLeftBlue = new Texture("left1b.png");
    static Texture bathubLeftPink = new Texture("left1p.png");

    private Texture bathubRightImg;
    static Texture bathubRightPink = new Texture("right1p.png");
    static Texture bathubRightBlue = new Texture("right1b.png");

    private QueueRenderer queueRenderer;
    private SpriteBatch batch;

    private Texture duckImg;
    private Texture duckNoticeImg;
    private Texture gameOverImg;

    private BitmapFont font;
    private int count;
    private Music music;
    boolean begin = false;
    private Texture correctImg = new Texture("correct.png");

    public WorldRenderer(BubbleGame bubbleGame, World world) {
        this.bubbleGame = bubbleGame;
        this.batch = bubbleGame.batch;
        this.world = world;
        this.font = new BitmapFont();
        this.font.getData().setScale(2, 2);
        this.font.setColor(com.badlogic.gdx.graphics.Color.RED);

        this.bubbleQueue = world.getBubbleQueue();
        this.bubbleQueue2 = world.getBubbleQueue2();
        this.bubbleImg = new Texture("bubbleblue.png");
        this.bubblePinkImg = new Texture("bubblepink.png");
        this.backgroundImg = new Texture("bathroom.jpg");
        this.titleImg = new Texture("title.jpg");

        this.bathubLeft = world.getBathubLeft();
        this.bathubRight = world.getBathubRight();
        this.bathubLeftImg = bathubLeftBlue;
        this.bathubRightImg = bathubRightBlue;
        this.duckImg = new Texture("duck.png");
        this.duckNoticeImg = new Texture("ducker.png");
        this.gameOverImg = new Texture("gameover.jpg");
        this.count = 0;
        this.music = Gdx.audio.newMusic(Gdx.files.internal("POL-starry-night.mp3"));

        this.queueRenderer = new QueueRenderer(this.batch, this.bubbleQueue, this.bubbleQueue2);
    }

    public void render(float delta) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            this.begin = true;
        }
        if (this.begin) {
            this.batch.begin();
            this.batch.draw(this.backgroundImg, 0, 0);
            if (World.getCountBub(this.count) <= 5) {
                this.font.draw(this.batch, "press f and j to receive bubble", 20, 210);
            }
            this.batch.end();
            this.world.updateQueueTimeLeft();
            this.world.updateQueueTimeRight();

            this.queueRenderer.render();
            updateColor();

            if (this.world.getTime() >= 0) {
                screen();
                checkBubbleIntoBath();
                musicPlay();
            } else {
                gameOverScreen();
            }
        } else {
            this.batch.begin();
            this.batch.draw(this.titleImg, 0, 0);
            this.font.draw(this.batch, "press spacebar to begin", 480, 50);
            this.batch.end();
        }
    }

    public void screen() {
        this.batch.begin();
        Vector2 postub = this.bathubLeft.getPosition();
        this.batch.draw(this.bathubLeftImg, postub.x, postub.y);
        Vector2 postub2 = this.bathubRight.getPosition();
        this.batch.draw(this.bathubRightImg, postub2.x, postub2.y);
        this.font.draw(this.batch, "Score :  " + this.world.getScore(), 620, 550);
        this.font.draw(this.batch, "Time :  " + this.world.getTime(), 620, 500);
        if (World.getCountBub(this.count) >= 90) {
            this.batch.draw(this.duckNoticeImg, 550, 10);
            if (World.getCountBub(this.count) % 2 == 0 && World.getCountBub(this.count) <= 100) {
                this.font.draw(this.batch, "receive opposite bubble color", 20, 210);
            }
        } else {
            this.batch.draw(this.duckImg, 550, 10);
        }
        if(this.world.getCorrect() == 1){
            this.batch.draw(this.correctImg, 400, 200);
        }
        this.batch.end();
    }

    public void gameOverScreen() {
        this.batch.begin();
        this.batch.draw(this.gameOverImg, 0, 0);
        this.font.draw(this.batch, "" + this.world.getScore(), 470, 238);
        this.batch.end();
    }

    public void checkBubbleIntoBath() {
        if (World.getCountBub(this.count) >= 90) {
            CheckColisionInverse.check(this.bubbleQueue, this.bathubLeft, this.world);
            CheckColisionInverse.check2(this.bubbleQueue2, this.bathubRight, this.world);
        } else {
            CheckColision.check(this.bubbleQueue, this.bathubLeft, this.world);
            CheckColision.check2(this.bubbleQueue2, this.bathubRight, this.world);
        }
    }

    public void musicPlay() {
        this.music.setLooping(true);
        this.music.setVolume(0.2f);
        this.music.play();
    }

    public void updateColor() {
        if (this.bathubLeft.getColor() == Color.Blue) {
            this.bathubLeftImg = bathubLeftBlue;
        }
        if (this.bathubLeft.getColor() == Color.Pink) {
            this.bathubLeftImg = bathubLeftPink;
        }
        if (this.bathubRight.getColor() == BathubRight.Color.Blue) {
            this.bathubRightImg = bathubRightBlue;
        }
        if (this.bathubRight.getColor() == BathubRight.Color.Pink) {
            this.bathubRightImg = this.bathubRightPink;
        }
    }
}
