package com.mygdx.game;

import java.util.Random;

import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.BathubLeft.Color;

public class Bubble {

    enum Color {
        Blue, Pink
    };

    private Vector2 position;
    private Color state;
    private int count = 0;

    public Bubble(float x, float y, Bubble.Color color) {
        this.position = new Vector2(x, y);
        this.state = color;
    }

    public Vector2 getPosition() {
        return this.position;
    }

    public Color getColor() {
        return this.state;
    }

    public void update() {
        move();
    }

    public void move() {
        if (World.getCountBub(this.count) >= 90) {
            this.position.y -= 5;
        } else if (World.getCountBub(this.count) >= 70) {
            this.position.y -= 9;
        } else if (World.getCountBub(this.count) >= 50) {
            this.position.y -= 7;
        } else if (World.getCountBub(this.count) >= 30) {
            this.position.y -= 6;
        } else {
            this.position.y -= 5;
        }
    }

    public static Bubble.Color getRandomColor() {
        Random random = new Random();
        int number = random.nextInt(100);
        if (number <= 50) {
            return Color.Blue;
        } else {
            return Color.Pink;
        }
    }
}