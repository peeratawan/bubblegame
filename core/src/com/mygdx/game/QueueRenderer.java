package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class QueueRenderer {

    private SpriteBatch batch;
    private BubbleQueue bubbleQueue;
    private BubbleQueue2 bubbleQueue2;
    private Texture bubbleImg;
    private Texture bubblePinkImg;

    public QueueRenderer(SpriteBatch batch, BubbleQueue bubbleQueue, BubbleQueue2 bubbleQueue2) {
        this.batch = batch;
        this.bubbleQueue = bubbleQueue;
        this.bubbleQueue2 = bubbleQueue2;
        this.bubbleImg = new Texture("bubbleblue.png");
        this.bubblePinkImg = new Texture("bubblepink.png");
    }

    public void render() {
        this.batch.begin();
        for (int i = this.bubbleQueue.getfront();; i++) {
            if (i == this.bubbleQueue.getrear()) {
                break;
            }
            if (i == this.bubbleQueue.getSizeOfQueue()) {
                i = -1;
                continue;
            }
            Vector2 pos = this.bubbleQueue.getBubbleAt(i).getPosition();
            if (pos.y >= 60) {
                if (this.bubbleQueue.getBubbleAt(i).getColor() == Bubble.Color.Blue) {
                    this.batch.draw(this.bubbleImg, pos.x, pos.y);
                } else {
                    this.batch.draw(this.bubblePinkImg, pos.x, pos.y);
                }
            }
        }
        for (int i = this.bubbleQueue2.getfront();; i++) {
            if (i == this.bubbleQueue2.getrear()) {
                break;
            }
            if (i == this.bubbleQueue2.getSizeOfQueue()) {
                i = -1;
                continue;
            }
            Vector2 pos = this.bubbleQueue2.getBubbleAt(i).getPosition();
            if (pos.y >= 60) {
                if (this.bubbleQueue2.getBubbleAt(i).getColor() == Bubble.Color.Blue) {
                    this.batch.draw(this.bubbleImg, pos.x, pos.y);
                } else {
                    this.batch.draw(this.bubblePinkImg, pos.x, pos.y);
                }
            }
        }
        this.batch.end();
    }
}
