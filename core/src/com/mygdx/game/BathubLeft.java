package com.mygdx.game;

import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.BathubLeft.Color;

public class BathubLeft {

    public enum Color {
        Blue, Pink
    };

    private Vector2 position;
    private Color state;

    public BathubLeft(int x, int y) {
        this.position = new Vector2(x, y);
        this.state = Color.Blue;
    }

    public Vector2 getPosition() {
        return this.position;
    }

    public Color getColor() {
        return this.state;
    }

    public void swapColor() {
        if (this.state == Color.Blue) {
            this.state = Color.Pink;
        } else {
            this.state = Color.Blue;
        }
    }
}
