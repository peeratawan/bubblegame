package com.mygdx.game;

import com.badlogic.gdx.math.Vector2;

public class BubbleQueue2 {

    public static final int sizeOfQueue = 7;
    private Bubble[] bubbles;
    int front;
    int rear;
    private Bubble bubble;

    public BubbleQueue2() {
        this.bubbles = new Bubble[sizeOfQueue];
        this.front = 0;
        this.rear = 0;
    }

    public void Insert(Bubble bubbles) {
        this.bubbles[this.rear] = bubbles;
        this.rear++;
        if (this.rear == sizeOfQueue) {
            this.rear = 0;
        }
        this.bubbles[this.rear] = null;
        if (this.rear == this.front) {
            this.front++;
        }
        if (this.front == sizeOfQueue) {
            this.front = 0;
        }
    }

    public void delete() {
        this.bubbles[this.front] = null;
        this.front++;
        if (this.front == sizeOfQueue) {
            this.front = 0;
        }
    }

    public void createBubble(float x, float y, Bubble.Color color) {
        this.bubble = new Bubble(x, y, color);
        Insert(this.bubble);
    }

    public int getfront() {
        return this.front;
    }

    public int getrear() {
        return this.rear;
    }

    public int getSizeOfQueue() {
        return this.sizeOfQueue;
    }

    public Vector2 getPosition() {
        return this.bubble.getPosition();
    }

    public Bubble getBubbleAt(int index) {
        return this.bubbles[index];
    }
}
