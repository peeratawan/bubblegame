package com.mygdx.game;

import com.badlogic.gdx.math.Vector2;

public class CheckColisionInverse {
    public static void check2(BubbleQueue2 bubbleQueue2, BathubRight bathubRight,World world){
        for(int j = bubbleQueue2.getfront(); ; j++){
            if(j == bubbleQueue2.getrear()){
                break;
            }
            if(j == bubbleQueue2.getSizeOfQueue()){
                j=-1;
                continue;
            }
            Vector2 posBubble2 = bubbleQueue2.getBubbleAt(j).getPosition();
            Vector2 posBathubRight = bathubRight.getPosition();
            if (posBubble2.y == 70  && bubbleQueue2.getBubbleAt(j).getColor() == Bubble.Color.Blue && bathubRight.getColor()== BathubRight.Color.Pink ){
                world.increaseScore();
            }
            else if(posBubble2.y == 70  && bubbleQueue2.getBubbleAt(j).getColor() == Bubble.Color.Pink && bathubRight.getColor()== BathubRight.Color.Blue){
                world.increaseScore();
            }
            else if(posBubble2.y == 70){
                world.decreaseTime();
                world.decreaseScore();
            }
            world.decreaseTime();
        }
    }
    public static void check(BubbleQueue bubbleQueue,BathubLeft bathubLeft,World world){
        for(int i = bubbleQueue.getfront(); ; i++){
            if(i == bubbleQueue.getrear()){
                break;
            }
            if(i == bubbleQueue.getSizeOfQueue()){
                i=-1;
                continue;
            }
            Vector2 posBubble = bubbleQueue.getBubbleAt(i).getPosition();
            Vector2 posBathubLeft = bathubLeft.getPosition();
            if(posBubble.y == 70  && bubbleQueue.getBubbleAt(i).getColor() == Bubble.Color.Blue && bathubLeft.getColor()== BathubLeft.Color.Pink){
                world.increaseScore();
            }
            else if(posBubble.y == 70   && bubbleQueue.getBubbleAt(i).getColor() == Bubble.Color.Pink && bathubLeft.getColor()== BathubLeft.Color.Blue){
                world.increaseScore();
            }
            else if(posBubble.y == 70) {
                world.decreaseTime();
                world.decreaseScore();
            }
            world.decreaseTime();
        }
    }
}


