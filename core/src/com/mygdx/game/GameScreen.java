package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class GameScreen extends ScreenAdapter {

    private BubbleGame bubbleGame;
    private Texture bubbleImg;
    private Bubble bubble;
    World world;
    private BathubLeft bathubLeft;
    private BathubRight bathubRight;
    private WorldRenderer worldRenderer;

    private double lastTimePressF;
    private double lastTimePressJ;

    public GameScreen(BubbleGame bubbleGame) {
        this.world = new World(bubbleGame);
        this.bathubLeft = this.world.getBathubLeft();
        this.bathubRight = this.world.getBathubRight();
        this.worldRenderer = new WorldRenderer(bubbleGame, this.world);
        double curTime = System.currentTimeMillis();
        this.lastTimePressF = curTime;
        this.lastTimePressJ = curTime;
    }

    @Override
    public void render(float delta) {
        getInput();
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        this.worldRenderer.render(delta);
    }

    private void getInput() {
        double curTime = System.currentTimeMillis();
        if (Gdx.input.isKeyJustPressed(Input.Keys.F)) {
            this.bathubLeft.swapColor();
            this.lastTimePressF = System.currentTimeMillis();
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.J)) {
            this.bathubRight.swapColor();
            this.lastTimePressJ = System.currentTimeMillis();
        }
    }
}
